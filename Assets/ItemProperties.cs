﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* 
public class ItemProperties : MonoBehaviour
{
    [Header("Mani consumables")]
    public string itemName;

    [SerializeField] private bool food;
    [SerializeField] private bool water;
    [SerializeField] private bool health;
    [SerializeField] public float value;

    [SerializeField] private playerVitals playerVitals;

    public void Interaction()
    {
        if(food)
        {
            playerVitals.hungerSlider.value += value;
        } else if(water)
        {
            playerVitals.thirstSlider.value += value;
        }

        else if(health)
        {
            playerVitals.healthSlider.value += value;
        }
    }



}

*/

public class ItemProperties : MonoBehaviour
{
    public string itemName;

    [Header("Your Consumables")]
    [SerializeField] private bool food;
    [SerializeField] private bool water;
    [SerializeField] private bool health;
    [SerializeField] private bool sleepingBag;
    [SerializeField] private float value;
    [SerializeField] private SleepController sleepController;
    void start()
    {
        sleepController = GameObject.FindGameObjectWithTag("SleepController").GetComponent<SleepController>();
    }

    public void Interaction (playerVitals playerVitals) // <-- Šis ir modificēts
    {
        if (food)
        {
            playerVitals.hungerSlider.value += value;
        }
        else if (water)
        {
            playerVitals.thirstSlider.value += value;
            //playerVitals.waterSlider.value += value;
        }
        else if (health)
        {
            playerVitals.healthSlider.value += value;
        }

        else if(sleepingBag)
        {
            sleepController.EnableSleepUI();
        }
    }
}
