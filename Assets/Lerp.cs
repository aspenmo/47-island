﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerp : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform mTarget;
    public float mTravelTime;
    Vector3 mStartingPos;
    Vector3 mTargetPos;
    float mTimer;
    // Start is called before the first frame update
    void Start()
    {
        mStartingPos = transform.position;
        mTargetPos = mTarget.position;
        mTimer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        mTimer += Time.deltaTime;
        transform.position = Vector3.Lerp(mStartingPos,mTargetPos,mTimer/mTravelTime);
    }
}
