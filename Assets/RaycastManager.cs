﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/* 
public class RaycastManager : MonoBehaviour
{
    private GameObject raycastedObj;

    [Header("Raycast uzstādījumi")]
    //var redzet private var inspectoraa
    [SerializeField] private float rayLength = 10;
    [SerializeField] private LayerMask newLayerMask;

    [Header("Atsauksmes")]
    [SerializeField] private Image crossHair;
    [SerializeField] private Text itemNameText;

    void Update()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);


        if(Physics.Raycast(transform.position, fwd, out hit, rayLength, newLayerMask.value))
        {
            //ko mes meklejam
            if(hit.collider.CompareTag("Consumable"))
            {
                CrosshairActive();
                //game object uz kuru mees gribam skatiities, ir jebkas, ko uzsitaam raycast
                raycastedObj = hit.collider.gameObject;
                //atjaunot UI vaardu
                itemNameText.text = raycastedObj.GetComponent<ItemProperties>().itemName;

                if(Input.GetMouseButton(0))
                {
                    //Objekta uzstadijumi
                    raycastedObj.GetComponent<ItemProperties>().Interaction();
                    raycastedObj.SetActive(false);
                }
            }
        }

        else
        {
            CrosshairNormal();
            //prieksmeta nosaukums atpakal uz normal
            itemNameText.text = null;
        }
    } 

    void CrosshairActive()
    {
        crossHair.color = Color.red;
    }

    void CrosshairNormal(){
        crossHair.color = Color.white;
    }

}

*/
public class RaycastManager : MonoBehaviour
{
    private GameObject raycastedObj;

    [Header("Raycast settings")]
    [SerializeField] private float rayLength = 10f;
    [SerializeField] private LayerMask hitMask;

    [Header("References")]
    [SerializeField] private playerVitals playerVitals; // <-- Šis ir jauns
    [SerializeField] private Image crossHair;
    [SerializeField] private Text itemNameText;

    private void Update()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast (transform.position, fwd, out hit, rayLength, hitMask))
        {
            if (hit.collider.CompareTag("Consumable"))
            {
                CrosshairActive();
                raycastedObj = hit.collider.gameObject;
                ItemProperties properties = raycastedObj.GetComponent<ItemProperties>(); // <-- Šī ir optimizēšana neliela
                itemNameText.text = properties.itemName;

                if (Input.GetMouseButtonDown(0))
                {
                    properties.Interaction(playerVitals); // <-- Šis ir modificēts
                    raycastedObj.SetActive(false);
                }
            }
        }
        else
        {
            CrosshairNormal();
            itemNameText.text = null;
        }
    }

    void CrosshairActive()
    {
        crossHair.color = Color.red;
    }
    void CrosshairNormal()
    {
        crossHair.color = Color.white;
    }
}