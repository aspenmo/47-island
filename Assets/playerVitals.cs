﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class playerVitals : MonoBehaviour
{
    //definejam sliderus
    #region Health Variables 
        //pieklustam sliderim
        public Slider healthSlider;
        //maksimala veseliiba, lai nevaram paarkaapt
        public int maxHealth;
        //cik aatri kritiisies veseliiba
        public int healthFallRate;
    #endregion
    #region Thirst Variables
    public Slider thirstSlider;
    public int maxThirst;
    public int thirstFallRate;
    #endregion
    #region Hunger Variables
    public Slider hungerSlider;
    public int maxHunger;
    public int hungerFallRate;
    #endregion
    #region Stamina Variables
    public Slider staminaSlider;
    public int normMaxStamina;
    public float fatMaxStamina;
    private int staminaFallRate;
    public int staminaFallMulti;
    private int staminaRegainRate;
    public int staminaRegainMulti;
    #endregion
    #region Fatigue Region
    [Space(10)]
    public Slider fatigueSlider;
    public int maxFatigue;
    public int fatigueFallRate;
    public bool fatStage1 = true;
    public bool fatStage2 = true;
    public bool fatStage3 = true;
    #endregion
    #region Temperature Variables
    [Header("Temperatūras uzstādījumi")]
    public float freezingTemp;
    public float currentTemp;
    public float normalTemp;
    public float heatTemp;
    public Text tempNumber;
    public Image tempBG;    
    #endregion
    #region Reference Variables
    private CharacterController charController;
    private FirstPersonController playerController;
    #endregion
    void Start()
    {
        #region Starting Sliders
        fatigueSlider.maxValue = maxHealth;
        fatigueSlider.value = maxFatigue;
        //saakuma veertiiba arii tiks uzstaadiita
        healthSlider.maxValue = maxHealth;
        //uzstaadiis max veseliibas skaitli un jebkaadu skaitli ko uzstaadiisim
        healthSlider.value = maxHealth;

        thirstSlider.maxValue = maxThirst;
        thirstSlider.value = maxThirst;

        hungerSlider.maxValue = maxHunger;
        hungerSlider.value = maxHunger;

        staminaSlider.maxValue = normMaxStamina;
        staminaSlider.value = normMaxStamina;

        staminaFallRate = 1;
        staminaRegainRate = 1;
        //meklejam character controllieri
        charController = GetComponent<CharacterController>();
        playerController = GetComponent<FirstPersonController>();
        #endregion
    }

    //skaitam lejaa
void UpdateTemp()
{
    tempNumber.text = currentTemp.ToString("00.0");
}

    void Update()
    {
        #region Fatigue Region
            if(fatigueSlider.value <= 60 && fatStage1)
            {
                fatMaxStamina = 80;
                staminaSlider.value = fatMaxStamina;
                fatStage1 = false;
            }

            else if(fatigueSlider.value <= 40 && fatStage2)
            {
                fatMaxStamina = 60;
                staminaSlider.value = fatMaxStamina;
                fatStage2 = false;
            }

            else if(fatigueSlider.value <= 20 && fatStage3)
            {
                fatMaxStamina = 20;
                staminaSlider.value = fatMaxStamina;
                fatStage3 = false;
            }

        if(fatigueSlider.value >= 0)
        {
            fatigueSlider.value -= Time.deltaTime / fatigueFallRate;
        }

        else if(fatigueSlider.value <= 0)
        {
            fatigueSlider.value = 0;
        }

        else if(fatigueSlider.value >= maxFatigue)
        {
            fatigueSlider.value = maxFatigue;
        }
        #endregion
        #region Temperature Region
        //TEMPERATURAS SEKCIJA
        if(currentTemp <= freezingTemp)
        {
            tempBG.color = Color.blue;
            UpdateTemp();

        }

        else if(currentTemp  >= heatTemp - 0.1) {
            tempBG.color = Color.red;
            UpdateTemp();
        }

        else
        {
            tempBG.color = Color.green;
            UpdateTemp();
        }
        #endregion
        #region Health Region
        //VESELIBAS KONTROLIERIS
        //Ja bada veertiiba (ko redzam "bar") un slaapes ir 0 arii 
        if(hungerSlider.value <= 0 && (thirstSlider.value <= 0))
        {
            //tad nodrosinam, ka dzivibas slideris skaita nost "freimus" - delta time, noraadot krituma aatruma
            //un reizinot ar divi, lai parliecinatos, ka tas ir atrak
            healthSlider.value -= Time.deltaTime / healthFallRate * 2;
        }


        else if (hungerSlider.value <= 0 || thirstSlider.value <= 0 || currentTemp <= freezingTemp || currentTemp >= heatTemp)
        {
            //nonemam *2, lai ilgaak
            healthSlider.value -= Time.deltaTime / healthFallRate;
        }

        if(healthSlider.value <= 0)
        {
            //izsaucam funkciju
            CharacterDeath();
        }
        #endregion
        #region Hunger Region
        //BADA KONTROLIERIS
        //ja ir palicis kaut kas bada "bar", skaitam nost
        if(hungerSlider.value >= 0)
        {
            hungerSlider.value -= Time.deltaTime / hungerFallRate;
        }

        //ja bads sasniedz vertibu nulle
        else if (hungerSlider.value <= 0)
        {
            //lai nevaram nekad iet zem nulles
            hungerSlider.value = 0;
        }

        else if(hungerSlider.value >= maxHunger)
        {
            hungerSlider.value = maxHunger;
        }
        #endregion
        #region Thirst Region
        //SLAAPJU KONTROLIERIS
        if(thirstSlider.value >= 0)
        {
            thirstSlider.value -= Time.deltaTime / thirstFallRate;
        }

        //ja bads sasniedz vertibu nulle
        else if (thirstSlider.value <= 0)
        {
            //lai nevaram nekad iet zem nulles
            thirstSlider.value = 0;
        }

        else if(thirstSlider.value >= maxThirst)
        {
            thirstSlider.value = maxThirst;
        }
        #endregion
        #region Stamina Region
        //IZTURIIBAS KONTROLIERIS
        //ja kustamies un nospiezam kreiso shift (get key vai turam), tatad sprintojam
        if(charController.velocity.magnitude > 0 && Input.GetKey(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.Space))
        {
            //samazinam izturiibu/staminu
            staminaSlider.value -= Time.deltaTime / staminaFallRate * staminaFallMulti;

            //ja ir kada stamina palikusi un mees skrienam
            if(staminaSlider.value > 0) {
                currentTemp += Time.deltaTime / 5;
            }
        }

        else
        {
            staminaSlider.value += Time.deltaTime / staminaRegainRate * staminaRegainMulti;

            if(currentTemp >= normalTemp)
            {
                currentTemp -= Time.deltaTime / 10;
            }
        }

        if(staminaSlider.value >= fatMaxStamina)
        {
            staminaSlider.value = fatMaxStamina;
        }

        //ja beidzaas izturiiba
        else if(staminaSlider.value <= 0)
        {
            staminaSlider.value = 0;
            //pieklustam FPS klasei, ja skrien uzstadam, ka stav
            playerController.m_RunSpeed = playerController.m_WalkSpeed;
        }

        else if(staminaSlider.value >= 0)
        {
            playerController.m_RunSpeed = playerController.m_RunSpeedNorm;
        }
        #endregion
    }

    #region Death Region
    void CharacterDeath()
    {
        //DARAM KAUT KO, KAD NOMIRST
    }
    #endregion
}
